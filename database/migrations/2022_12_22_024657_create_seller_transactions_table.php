<?php

use App\Models\SellerTransaction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create( 'seller_transactions', function ( Blueprint $table ) {
            $table->id();
            $table->foreignId( 'shop_id' )->constrained( 'shops' )->cascadeOnDelete()->cascadeOnUpdate();
            $table->tinyInteger( 'status' )->default( SellerTransaction::STATUS_OPEN );
            $table->string( 'refrence_id' )->nullable()->unique();
            $table->bigInteger( 'price' );
            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists( 'seller_transactions' );
    }
};
