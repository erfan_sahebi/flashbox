<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create( 'order_metas', function ( Blueprint $table ) {
            $table->id();
            $table->foreignId( 'order_id' )->constrained( 'orders' )->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignId( 'product_id' )->constrained( 'products' )->cascadeOnDelete()->cascadeOnUpdate();
            $table->bigInteger( 'price' )->nullable();
            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists( 'order_metas' );
    }
};
