<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create( 'order_meta_seller_transaction', function ( Blueprint $table ) {
            $table->id();
            $table->foreignId( 'order_meta_id' )->constrained( 'order_metas' )->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignId( 'seller_transaction_id' )->constrained( 'seller_transactions' )->cascadeOnDelete()->cascadeOnUpdate();
            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists( 'order_meta_seller_transaction' );
    }
};
