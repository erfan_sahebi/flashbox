<?php

namespace Database\Seeders;

use App\DAL\ProductDAL;
use App\DAL\SellerTransactionDAL;
use App\DAL\TransactionDAL;
use App\Models\SellerTransaction;
use App\Models\Transaction;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TransactionSeeder extends Seeder
{
    private TransactionDAL $transactionDAL;

    private SellerTransactionDAL $sellerTransactionDAL;

    private ProductDAL $productDAL;

    public function __construct ( TransactionDAL $transactionDAL, SellerTransactionDAL $sellerTransactionDAL, ProductDAL $productDAL )
    {
        $this->transactionDAL = $transactionDAL;

        $this->sellerTransactionDAL = $sellerTransactionDAL;

        $this->productDAL = $productDAL;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        $this->transactionDAL->create( [
            'order_id'    => 1,
            'price'       => 6500000,
            'refrence_id' => Str::uuid()->toString(),
            'status'      => Transaction::STATUS_SUCCESS,
        ] );

        /**
         * @var SellerTransaction $seller_transaction
         */
        $seller_transaction = $this->sellerTransactionDAL->create( [
            'shop_id' => 1,
            'status'  => SellerTransaction::STATUS_OPEN,
            'price'   => 6500000,
        ] );

        $seller_transaction->orderMetas()->sync( $this->productDAL->all() );
    }
}
