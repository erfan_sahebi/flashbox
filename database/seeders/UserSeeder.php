<?php

namespace Database\Seeders;

use App\DAL\RoleDAL;
use App\DAL\UserDAL;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{

    protected UserDAL $userDAL;
    protected RoleDAL $roleDAL;

    public function __construct ( UserDAL $userDAL, RoleDAL $roleDAL )
    {
        $this->userDAL = $userDAL;
        $this->roleDAL = $roleDAL;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        /**
         * @var User $admin
         */
        $admin = $this->userDAL->create( [
            'name'     => 'Admin',
            'email'    => 'admin_erfansahebivayghan@gmail.com',
            'password' => Hash::make( '123456789' )
        ] );
        $this->roleDAL->assignRoleToUser( $admin, Role::ADMIN_ROLE_NAME );

        /**
         * @var User $seller
         */
        $seller = $this->userDAL->create( [
            'name'     => 'Seller',
            'email'    => 'seller_erfansahebivayghan@gmail.com',
            'password' => Hash::make( '123456789' )
        ] );
        $this->roleDAL->assignRoleToUser( $seller, Role::SELLER_ROLE_NAME );

        /**
         * @var User $customer
         */
        $customer = $this->userDAL->create( [
            'name'     => 'Customer',
            'email'    => 'customer_erfansahebivayghan@gmail.com',
            'password' => Hash::make( '123456789' )
        ] );
        $this->roleDAL->assignRoleToUser( $customer, Role::CUSTOMER_ROLE_NAME );
    }
}
