<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderMeta extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'order_id',
        'product_id',
        'price',
    ];

    public function order (): BelongsTo
    {
        return $this->belongsTo( Order::class );
    }

    public function product (): BelongsTo
    {
        return $this->belongsTo( Product::class );
    }
}
