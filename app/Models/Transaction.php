<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use HasFactory, SoftDeletes;

    const STATUS_FAILED = 0;
    const STATUS_SUCCESS = 1;

    protected $fillable = [
        'order_id',
        'price',
        'refrence_id',
        'status',
    ];

    public function order (): BelongsTo
    {
        return $this->belongsTo( Order::class );
    }
}
