<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class SellerTransaction extends Model
{
    use HasFactory, SoftDeletes;

    const STATUS_FAILED = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_OPEN = 2;

    protected $fillable = [
        'shop_id',
        'status',
        'refrence_id',
        'price',
    ];

    public function shop (): BelongsTo
    {
        return $this->belongsTo( Shop::class );
    }

    public function orderMetas (): BelongsToMany
    {
        return $this->belongsToMany( OrderMeta::class );
    }
}
