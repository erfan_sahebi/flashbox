<?php

namespace App\Models;

use Spatie\Permission\Models\Role as RoleModel;

class Role extends RoleModel
{
    const ADMIN_ROLE_NAME = 'admin';
    const SELLER_ROLE_NAME = 'seller';
    const CUSTOMER_ROLE_NAME = 'customer';
}
