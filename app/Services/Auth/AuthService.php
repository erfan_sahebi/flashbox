<?php

namespace App\Services\Auth;

use App\DAL\PermissionDAL;
use App\DAL\RoleDAL;
use App\DAL\UserDAL;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\PersonalAccessTokenResult;
use Nette\Schema\ValidationException;

class AuthService implements AuthServiceInterface
{
    private UserDAL $userDAL;
    private RoleDAL $roleDAL;
    private PermissionDAL $permissionDAL;

    const TOKEN_NAME = 'UserToken';

    public function __construct ( UserDAL $userDAL, RoleDAL $roleDAL, PermissionDAL $permissionDAL )
    {
        $this->userDAL       = $userDAL;
        $this->roleDAL       = $roleDAL;
        $this->permissionDAL = $permissionDAL;
    }

    protected function tokenResource ( PersonalAccessTokenResult $token, User $user ): array
    {
        return [
            'user_id'    => $user->id,
            'token'      => $token->accessToken,
            'expires_at' => $token->token[ 'expires_at' ]
        ];
    }

    protected function createToken ( User $user, string $name = self::TOKEN_NAME ): PersonalAccessTokenResult
    {
        return $user->createToken( $name );
    }

    public function register ( User $user ): array
    {
        $this->userDAL->beginTransaction();

        try
        {
            /**
             * @var User $user
             */
            $user = $this->userDAL->save( $user );

            $this->assignRoleToUser( $user, Role::CUSTOMER_ROLE_NAME );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->userDAL->rollback();

            throw $exception;
        }

        $this->userDAL->commit();

        $token = $this->createToken( $user );

        return $this->tokenResource( $token, $user );
    }

    public function login ( string $email, string $hashed_password ): array
    {
        /**
         * @var User $user
         */
        $user = $this->userDAL->fetchByColumn( 'email', $email );

        if ( ! Hash::check( $hashed_password, $user->password ) )
        {
            throw new \Exception( LoginRequest::EXISTS_EMAIL_MESSAGE );
        }

        $token = $this->createToken( $user );

        return $this->tokenResource( $token, $user );
    }

    # Role And Permissions

    public function createRole ( Role $role ): Role
    {

        try
        {
            /**
             * @var Role $role
             */
            $role = $this->roleDAL->save( $role );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->roleDAL->rollback();
            throw $exception;
        }

        $this->roleDAL->commit();

        return $role;
    }

    public function createPermission ( Permission $permission ): Permission
    {
        $this->permissionDAL->beginTransaction();

        try
        {
            /**
             * @var Permission $permission
             */
            $permission = $this->permissionDAL->save( $permission );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->permissionDAL->rollback();

            throw $exception;
        }

        $this->permissionDAL->commit();

        return $permission;
    }

    public function assignRoleToUser ( User $user, string...$role ): void
    {
        $this->roleDAL->beginTransaction();

        try
        {
            $this->roleDAL->assignRoleToUser( $user, ...$role );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->roleDAL->rollback();

            throw $exception;
        }

        $this->roleDAL->commit();
    }

    public function assignPermissionToRole ( Role $role, string ...$permission ): void
    {
        $this->roleDAL->beginTransaction();

        try
        {
            $this->roleDAL->givePermissionToRole( $role, ...$permission );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->roleDAL->rollback();

            throw $exception;
        }

        $this->roleDAL->commit();
    }

    public function logout ( User $user ): void
    {
        $user->token()->revoke();
    }
}
