<?php

namespace App\Services\Category;

use App\Models\Category;

interface CategoryServiceInterface
{
    public function list (): array;

    public function create ( Category $category ): Category;

    public function find ( int $id ): Category;

    public function delete ( int $id ): bool;

    public function update ( Category $category ): Category;
}
