<?php

namespace App\Services\Category;

use App\DAL\CategoryDAL;
use App\Models\Category;
use Nette\Schema\ValidationException;

class CategoryService implements CategoryServiceInterface
{

    private CategoryDAL $categoryDAL;

    public function __construct ( CategoryDAL $categoryDAL )
    {
        $this->categoryDAL = $categoryDAL;
    }

    public function list (): array
    {
        return $this->categoryDAL->all();
    }

    public function create ( Category $category ): Category
    {

        try
        {
            /**
             * @var Category $category
             */
            $category = $this->categoryDAL->save( $category );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->categoryDAL->rollback();

            throw $exception;
        }

        $this->categoryDAL->commit();

        return $category;
    }

    public function find ( int $id ): Category
    {
        try
        {
            /**
             * @var Category $category
             */
            $category = $this->categoryDAL->fetchByID( $id );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->categoryDAL->rollback();

            throw $exception;
        }

        $this->categoryDAL->commit();

        return $category;
    }

    public function delete ( int $id ): bool
    {
        try
        {
            $deleted = $this->categoryDAL->delete( $id );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->categoryDAL->rollback();

            throw $exception;
        }

        $this->categoryDAL->commit();

        return $deleted;
    }

    public function update ( Category $category ): Category
    {
        try
        {
            /**
             * @var Category $category
             */
            $category = $this->categoryDAL->update( $category );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->categoryDAL->rollback();

            throw $exception;
        }

        $this->categoryDAL->commit();

        return $category;
    }
}
