<?php

namespace App\Services\Order;

use App\Models\Order;
use App\Models\OrderMeta;
use App\Models\User;

interface OrderServiceInterface
{
    public function list ( User $user ): array;

    public function create ( Order $order, array $product_ids ): array;

    public function find ( int $id ): Order;

    public function delete ( int $id ): bool;

    public function update ( Order $order ): Order;

    public function createMeta ( OrderMeta $orderMeta ): OrderMeta;

    public function findMeta ( int $meta_id ): OrderMeta;

    public function deleteMeta ( int $meta_id ): bool;

    public function checkUserOrderPermission ( User $user, Order $order, string $permission ): bool;

    public function checkPermissionForAddOrderMeta ( int $order_id, int $product_id ): bool;
}
