<?php

namespace App\Services\Order;

use App\DAL\OrderDAL;
use App\DAL\OrderMetaDAL;
use App\Models\Order;
use App\Models\OrderMeta;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Nette\Schema\ValidationException;

class OrderService implements OrderServiceInterface
{
    private OrderDAL $orderDAL;
    private OrderMetaDAL $orderMetaDAL;

    public function __construct ( OrderDAL $orderDAL, OrderMetaDAL $orderMetaDAL )
    {
        $this->orderDAL     = $orderDAL;
        $this->orderMetaDAL = $orderMetaDAL;
    }

    public function list ( User $user ): array
    {
        if ( $user->hasRole( Role::ADMIN_ROLE_NAME ) )
        {
            $orders = $this->orderDAL->all();
        }
        else
        {
            $orders = $this->orderDAL->all( [
                [
                    'user_id',
                    $user->id
                ]
            ] );
        }

        return $orders;
    }

    public function create ( Order $order, array $product_ids ): array
    {
        $this->orderDAL->beginTransaction();

        try
        {
            $order->status = Order::STATUS_OPEN;

            /**
             * @var Order $order
             */
            $order = $this->orderDAL->save( $order );

            $current_time = Carbon::now();
            $order_metas  = [];
            foreach ( $product_ids as $product_id )
            {
                $order_metas[] = [
                    'order_id'   => $order->id,
                    'product_id' => $product_id,
                    'price'      => null,
                    'created_at' => $current_time,
                    'updated_at' => $current_time,
                ];
            }

            $this->orderMetaDAL->insert( $order_metas );

        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->orderDAL->rollback();

            throw $exception;
        }

        $this->orderDAL->commit();

        return [
            'order'       => $order,
            'order_metas' => $this->orderMetaDAL->fetchListByOrderID( $order->id ),
        ];

    }

    public function find ( int $id ): Order
    {
        /**
         * @var Order $order
         */
        $order = $this->orderDAL->fetchByIDWithMetas( $id );

        return $order;
    }

    public function delete ( int $id ): bool
    {
        $this->orderDAL->beginTransaction();

        try
        {
            $deleted = $this->orderDAL->delete( $id );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->orderDAL->rollback();

            throw $exception;
        }

        $this->orderDAL->commit();

        return $deleted;
    }

    public function update ( Order $order ): Order
    {
        $this->orderDAL->beginTransaction();

        try
        {
            /**
             * @var Order $order
             */
            $order = $this->orderDAL->save( $order );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->orderDAL->rollback();

            throw $exception;
        }

        $this->orderDAL->commit();

        return $order;
    }

    public function createMeta ( OrderMeta $orderMeta ): OrderMeta
    {
        $this->orderMetaDAL->beginTransaction();

        try
        {
            /**
             * @var OrderMeta $orderMeta
             */
            $orderMeta = $this->orderMetaDAL->save( $orderMeta );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->orderMetaDAL->rollback();

            throw $exception;
        }

        $this->orderMetaDAL->commit();

        return $orderMeta;
    }

    public function findMeta ( int $meta_id ): OrderMeta
    {
        /**
         * @var OrderMeta $order_meta
         */
        $order_meta = $this->orderMetaDAL->fetchByID( $meta_id );

        return $order_meta;
    }

    public function deleteMeta ( int $meta_id ): bool
    {
        $this->orderMetaDAL->beginTransaction();

        try
        {
            $deleted = $this->orderMetaDAL->delete( $meta_id );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->orderMetaDAL->rollback();

            throw $exception;
        }

        $this->orderMetaDAL->commit();

        return $deleted;
    }

    public function checkUserOrderPermission ( User $user, Order $order, string $permission ): bool
    {
        if ( $order->user_id != $user->id )
        {
            return false;
        }

        return true;
    }

    public function checkPermissionForAddOrderMeta ( int $order_id, int $product_id ): bool
    {
        return $this->orderMetaDAL->checkOrderIDWithProductIDExists( $order_id, $product_id );
    }


}
