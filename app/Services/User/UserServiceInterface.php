<?php

namespace App\Services\User;

use App\Models\User;

interface UserServiceInterface
{
    public function list (): array;

    public function createSeller ( User $user ): User;

    public function find ( int $id ): User;

    public function update ( User $user ): User;

    public function delete ( int $id ): bool;
}
