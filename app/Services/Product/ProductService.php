<?php

namespace App\Services\Product;

use App\DAL\ProductDAL;
use App\Models\Product;
use App\Models\Role;
use App\Models\User;
use Nette\Schema\ValidationException;

class ProductService implements ProductServiceInterface
{
    private ProductDAL $productDAL;

    public function __construct ( ProductDAL $productDAL )
    {
        $this->productDAL = $productDAL;
    }

    public function create ( Product $product ): Product
    {
        $this->productDAL->beginTransaction();

        try
        {
            /**
             * @var Product $product
             */
            $product = $this->productDAL->save( $product );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->productDAL->rollback();

            throw $exception;
        }

        $this->productDAL->commit();

        return $product;
    }

    public function find ( int $id ): Product
    {
        /**
         * @var Product $product
         */
        $product = $this->productDAL->fetchByID( $id );

        return $product;
    }

    public function update ( Product $product ): Product
    {
        $this->productDAL->beginTransaction();

        try
        {
            /**
             * @var Product $product
             */
            $product = $this->productDAL->update( $product );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->productDAL->rollback();

            throw $exception;
        }

        $this->productDAL->commit();

        return $product;
    }

    public function delete ( int $id ): bool
    {
        $this->productDAL->beginTransaction();

        try
        {
            $deleted = $this->productDAL->delete( $id );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->productDAL->rollback();

            throw $exception;
        }

        $this->productDAL->commit();

        return $deleted;
    }

    public function list (): array
    {
        return $this->productDAL->all();
    }
}
