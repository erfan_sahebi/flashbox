<?php

namespace App\Services\Product;

use App\Models\Product;

interface ProductServiceInterface
{
    public function create ( Product $product ): Product;

    public function find ( int $id ): Product;

    public function update ( Product $product ): Product;

    public function delete ( int $id ): bool;

    public function list (): array;
}
