<?php

namespace App\DAL;

use App\Models\OrderMeta;

class OrderMetaDAL extends BaseDAL
{

    protected function model (): string
    {
        return OrderMeta::class;
    }

    public function fetchListByOrderID ( int $order_id )
    {
        return $this->model->with('product')->where( 'order_id', $order_id )->get();
    }

    public function checkOrderIDWithProductIDExists ( int $order_id, int $product_id ): bool
    {
        return $this->model->where( 'order_id', $order_id )->where( 'product_id', $product_id )->exists();
    }
}
