<?php

namespace App\DAL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

abstract class BaseDAL
{
    protected Model $model;

    public function __construct ()
    {
        $this->model = app( $this->model() );
    }

    abstract protected function model (): string;

    public function beginTransaction (): void
    {
        DB::beginTransaction();
    }

    public function rollback (): void
    {
        DB::rollBack();
    }

    public function commit (): void
    {
        DB::commit();
    }

    public function all ()
    {
        return $this->model->all();
    }

    public function fetchByID ( int $id ): Model
    {
        return $this->model->findOrFail( $id );
    }

    public function create ( array $data ): Model
    {
        return $this->model->create( $data );
    }

    public function insert ( array $data ): bool
    {
        return $this->model->insert( $data );
    }

    public function save ( Model $model ): Model
    {
        $model->save();

        return $model;
    }

    public function delete ( int $id ): bool
    {
        return $this->model->destroy( $id );
    }

    public function whereColumn ( array $condition )
    {
        return $this->model->where( $condition )->get();
    }

    public function fetchByColumn ( string $key, string $value ): Model
    {
        return $this->model->where( $key, $value )->first();
    }

    public function update ( Model $model ): Model
    {
        return $this->save( $model );
    }
}
