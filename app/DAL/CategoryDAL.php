<?php

namespace App\DAL;

use App\Models\Category;

class CategoryDAL extends BaseDAL
{

    protected function model (): string
    {
        return Category::class;
    }
}
