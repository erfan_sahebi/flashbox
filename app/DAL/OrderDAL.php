<?php

namespace App\DAL;

use App\Models\Order;

class OrderDAL extends BaseDAL
{

    protected function model (): string
    {
        return Order::class;
    }

    public function fetchByIDWithMetas ( int $id )
    {
        return $this->model->with( 'metas' )->findOrFail( $id );
    }

    public function all ( array $condition = null )
    {
        if ( empty( $condition ) )
        {
            return $this->model->with( 'user' )->get();
        }

        return $this->model->with( 'user' )->where( $condition )->get();
    }
}
