<?php

namespace App\Rules\User;

use App\DAL\UserDAL;

class UpdateUserEmailRule extends BaseUserRule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes ( $attribute, $value )
    {
        parent::passes( $attribute, $value );

        $userDAL = new UserDAL();

        $target_user = $userDAL->fetchByColumn( 'email', $value );

        if ( ! empty( $target_user ) && $target_user->id != $this->user->id )
        {
            return false;
        }

        return true;
    }
}
