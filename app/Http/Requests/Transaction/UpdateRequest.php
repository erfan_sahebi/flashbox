<?php

namespace App\Http\Requests\Transaction;

use App\Models\Transaction;

class UpdateRequest extends FetchRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules ()
    {
        return array_merge( parent::rules(), [
            'refrence_id' => 'nullable|string|unique:transactions,refrence_id',
            'status'      => 'required|integer|in:' . implode( ',', [
                    Transaction::STATUS_FAILED,
                    Transaction::STATUS_SUCCESS,
                ] ),
        ] );
    }
}
