<?php

namespace App\Http\Requests\Shop;

use Illuminate\Foundation\Http\FormRequest;

class ListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules ()
    {
        $geo_validator = "/^\d*(\.\d{0,6})?$/";

        return [
            'lat'    => 'required|regex:' . $geo_validator,
            'long'   => 'required|regex:' . $geo_validator,
            'radius' => 'required|regex:' . $geo_validator,
        ];
    }

    protected function prepareForValidation ()
    {
        $this->merge( [
            'lat'    => $this->query( 'lat' ),
            'long'   => $this->query( 'long' ),
            'radius' => $this->query( 'radius' ),
        ] );
    }
}
