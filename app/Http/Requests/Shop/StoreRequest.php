<?php

namespace App\Http\Requests\Shop;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules ()
    {
        //        $geo_validator = "^(?:\d{0,9}\.\d{1,2})$|^\d{0,6}$";
        $geo_validator = "/^\d*(\.\d{0,6})?$/";

        return [
            'name' => 'required|string|max:255',
            'lat'  => 'required|regex:' . $geo_validator,
            'long' => 'required|regex:' . $geo_validator,
        ];
    }
}
