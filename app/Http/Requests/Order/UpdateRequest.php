<?php

namespace App\Http\Requests\Order;

use App\Models\Order;

class UpdateRequest extends FetchRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules ()
    {
        $geo_validator = "/^\d*(\.\d{0,6})?$/";

        return array_merge( parent::rules(), [
            'status' => 'required|integer|in:' . implode( ',', [
                    Order::STATUS_OPEN,
                ] ),
            'lat'    => 'required|regex:' . $geo_validator,
            'long'   => 'required|regex:' . $geo_validator,
        ] );
    }
}
