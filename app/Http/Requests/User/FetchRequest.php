<?php

namespace App\Http\Requests\User;

use App\Rules\User\FetchUserRule;
use Illuminate\Foundation\Http\FormRequest;

class FetchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules ()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:users',
                new FetchUserRule
            ],
        ];
    }

    public function all ( $keys = null )
    {
        return array_replace_recursive( parent::all(), $this->route()->parameters() );
    }
}
