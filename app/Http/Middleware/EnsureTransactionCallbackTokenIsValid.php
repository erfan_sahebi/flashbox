<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EnsureTransactionCallbackTokenIsValid
{
    private string $token;

    public function __construct ()
    {
        $this->token = config( 'transaction.token' );
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     *
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle ( Request $request, Closure $next )
    {
        if ($request->hasHeader('x-token') && $request->header('x-token') != $this->token)
        {

        }

        return $next( $request );
    }
}
