<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\Auth\AuthService;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    private AuthService $authService;

    public function __construct ( AuthService $authService )
    {
        $this->authService = $authService;
    }

    public function register ( RegisterRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $user           = new User();
        $user->name     = $validated_data[ 'name' ];
        $user->email    = $validated_data[ 'email' ];
        $user->password = Hash::make( $validated_data[ 'password' ] );

        $result = $this->authService->register( $user );

        return response()->json( $result, 201 );
    }

    public function login ( LoginRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $result = $this->authService->login( $validated_data[ 'email' ], $validated_data[ 'password' ] );

        return response()->json( $result );
    }

    public function logout (): JsonResponse
    {
        $this->authService->logout( Auth::user() );

        return response()->json( null, 204 );
    }

}
