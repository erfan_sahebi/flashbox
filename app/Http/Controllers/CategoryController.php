<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\FetchRequest;
use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use App\Http\Resources\Category\CategoryResource;
use App\Models\Category;
use App\Services\Category\CategoryService;
use Illuminate\Http\JsonResponse;

class CategoryController extends Controller
{

    private CategoryService $categoryService;

    public function __construct ( CategoryService $categoryService )
    {
        $this->categoryService = $categoryService;
    }

    public function list (): JsonResponse
    {
        $categories = $this->categoryService->list();

        return response()->json( CategoryResource::collection( $categories ) );
    }

    public function store ( StoreRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $category       = new Category();
        $category->name = $validated_data[ 'name' ];

        $category = $this->categoryService->create( $category );

        return response()->json( CategoryResource::make( $category ), 201 );
    }

    public function show ( FetchRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $category = $this->categoryService->find( $validated_data[ 'id' ] );

        return response()->json( CategoryResource::make( $category ) );
    }

    public function delete ( FetchRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $this->categoryService->delete( $validated_data[ 'id' ] );

        return response()->json( null, 204 );
    }

    public function update ( UpdateRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $shop       = new Category();
        $shop->id   = $validated_data[ 'id' ];
        $shop->name = $validated_data[ 'name' ];

        $category = $this->categoryService->update( $shop );

        return response()->json( CategoryResource::make( $category ) );
    }

}
