<?php

namespace App\Http\Controllers;

use App\DAL\UserDAL;
use App\Http\Requests\User\DeleteRequest;
use App\Http\Requests\User\FetchRequest;
use App\Http\Requests\User\StoreSellerRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Http\Resources\User\UserResource;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Services\User\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Exceptions\PermissionDoesNotExist;

class UserController extends Controller
{
    private UserService $userService;

    public function __construct ( UserService $userService )
    {
        $this->userService = $userService;
    }

    public function list (): JsonResponse
    {
        $users = $this->userService->list();

        return response()->json( UserResource::collection( $users ) );
    }

    public function storeSeller ( StoreSellerRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $seller           = new User();
        $seller->name     = $validated_data[ 'name' ];
        $seller->email    = $validated_data[ 'email' ];
        $seller->password = Hash::make( $validated_data[ 'password' ] );

        $seller = $this->userService->createSeller( $seller );

        return response()->json( UserResource::make( $seller ), 204 );
    }

    public function show ( FetchRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $target_user = $this->userService->find( $validated_data[ 'id' ] );

        return response()->json( UserResource::make( $target_user ) );
    }

    public function update ( UpdateRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $target_user        = $this->userService->find( $validated_data[ 'id' ] );
        $target_user->name  = $validated_data[ 'name' ];
        $target_user->email = $validated_data[ 'email' ];

        $target_user = $this->userService->update( $target_user );

        return response()->json( UserResource::make( $target_user ) );

    }

    public function delete ( DeleteRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $this->userService->delete( $validated_data[ 'id' ] );

        return response()->json( null, 204 );
    }
}
