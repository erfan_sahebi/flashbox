<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderMeta\DeleteRequest;
use App\Http\Requests\OrderMeta\StoreRequest;
use App\Http\Resources\Order\OrderMetaResource;
use App\Models\Order;
use App\Models\OrderMeta;
use App\Models\Permission;
use App\Services\Order\OrderService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Exceptions\PermissionDoesNotExist;

class OrderMetaController extends Controller
{
    private OrderService $orderService;

    public function __construct ( OrderService $orderService )
    {
        $this->orderService = $orderService;
    }

    public function store ( StoreRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $order = $this->orderService->find( $validated_data[ 'id' ] );

        if ( ! $this->orderService->checkUserOrderPermission( Auth::user(), $order, Permission::PERMISSIONS[ 'create_order_meta' ] ) )
            return response()->json( null, 201 );

        if ( $order->status != Order::STATUS_OPEN )
            return response()->json( null, 202 );

        if ( ! $this->orderService->checkPermissionForAddOrderMeta( $order->id, $validated_data[ 'product_id' ] ) )
            return response()->json( null, 202 );

        if ( ! $this->orderService->checkUserOrderPermission( Auth::user(), $order, Permission::PERMISSIONS[ 'create_order_meta' ] ) || $order->status != Order::STATUS_OPEN || ! $this->orderService->checkPermissionForAddOrderMeta( $order->id, $validated_data[ 'product_id' ] ) )
        {
            throw PermissionDoesNotExist::create( Permission::PERMISSIONS[ 'create_order_meta' ], 'api' );
        }

        $order_meta             = new OrderMeta();
        $order_meta->order_id   = $order->id;
        $order_meta->product_id = $validated_data[ 'product_id' ];

        $order_meta = $this->orderService->createMeta( $order_meta );

        return response()->json( OrderMetaResource::make( $order_meta ), 201 );

    }

    public function delete ( DeleteRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $order = $this->orderService->find( $validated_data[ 'id' ] );

        $order_meta = $this->orderService->findMeta( $validated_data[ 'meta_id' ] );

        if ( ! $this->orderService->checkUserOrderPermission( Auth::user(), $order, Permission::PERMISSIONS[ 'delete_order_meta' ] ) || $order_meta->order_id != $order->id )
        {
            throw PermissionDoesNotExist::create( Permission::PERMISSIONS[ 'delete_order_meta' ], 'api' );
        }

        $this->orderService->deleteMeta( $order_meta->id );

        return response()->json( null, 204 );
    }
}
